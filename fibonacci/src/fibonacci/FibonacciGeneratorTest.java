package fibonacci;

import java.util.Scanner;


public class FibonacciGeneratorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FibonacciGenerator fibonacciGenerator = new FibonacciGenerator();
		System.out.println("this is a program for caculate the Fibonacci sequence\n"
				+ "please enter a integer number bigger than 0: ");
		Scanner in = new Scanner(System.in);
		String enterString = in.next();
		if(!fibonacciGenerator.validateN(enterString))
		{
			System.out.println("please check your entries again");
		}
		else 
		{
			System.out.println(fibonacciGenerator.calculateNthValue(Integer.parseInt(enterString))+" is the result of "+enterString+"th value in the Fibonacci sequence ");
		}
		
		
		
	}

}
