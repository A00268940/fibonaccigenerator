package fibonacci;

public class FibonacciGenerator {

	public boolean validateN(String str)
	{
		for (int i = 0; i < str.length(); i++)
		{
			if (!Character.isDigit(str.charAt(i)))
			{
				return false;
			}

		}
		if(Integer.parseInt(str) <= 0)
		{
			return false;
		}
		return true;
	}
	
	public int calculateNthValue(int n)
	{
		if(n == 1 || n == 2)
		{
			return 1;
		}
		else
		{
			return calculateNthValue(n-1) + calculateNthValue(n-2);
		}
	}
	
}
